
public class VegetableSimulation {
    
     
    public static void main(String[] args) {
    
        VegetableFactory factory = VegetableFactory.getInstance();
        Vegetable carrot = factory.getVegetable(VegetableType.CARROT, "orange", 1.5);
        Vegetable carrotNotRipe = factory.getVegetable(VegetableType.CARROT,"orange",4);
        Vegetable beet = factory.getVegetable(VegetableType.BEET,"red",2);
        Vegetable beetNotRipe = factory.getVegetable(VegetableType.BEET,"red",1);
        
        
        
        System.out.println(" Color of "+carrot.getClass().getName()+ " is: "+carrot.getColor() + " and it is ripe: "+carrot.isRipe());
        System.out.println(" Color of "+carrotNotRipe.getClass().getName()+ " is: "+carrotNotRipe.getColor() + " and it is ripe: "+carrotNotRipe.isRipe());
        System.out.println(" Color of "+beet.getClass().getName()+ " is: "+beet.getColor() + " and it is ripe: "+beet.isRipe());
        System.out.println(" Color of "+beetNotRipe.getClass().getName()+ " is: "+beetNotRipe.getColor() + " and it is ripe: "+beetNotRipe.isRipe());
       
        
    
    
    }
}
