/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */


/**
 *
 * @author mymac
 */
public abstract class Vegetable {
    
    private String color;
    private double size;
    private VegetableType vegetableType;
   
    
    
    Vegetable(String color, double size){
        this.color = color;
        this.size = size;
    }
    
    public VegetableType getVegetableType(){
        return this.vegetableType;
    }
    
    public String getColor(){
        return this.color;
    }
    
    public double getSize(){
        return this.size;
    }
    
    public abstract boolean isRipe();
    
}
