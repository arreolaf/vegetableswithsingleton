
public class Beet extends Vegetable {
    private String name;
    Beet(String color,double size, String name){
    super(color,size);
    this.name = name;
    }

    Beet(String color, double size) {
        super(color,size);
    }
    public String getName(){
        return name;
    }
    
    public String getColor(){
        return super.getColor();
    }
    
    public double getSize(){
        return super.getSize();
    }
    
    @Override
    public boolean isRipe() {
        return getSize() == 2 && getColor().equalsIgnoreCase("red");
    
    }
    
     public String toString(){
        return "Name is: " + getName() + " ,color is: " + getColor() +" the size is: "+ getSize()+"cm" +" ,it is ripe: "+ isRipe();
    }
}
