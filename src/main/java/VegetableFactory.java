
public class VegetableFactory {
    
    private static VegetableFactory instance;
    
    VegetableFactory(){};
    
    
    public static VegetableFactory getInstance(){
        
        if(instance == null){
            instance = new VegetableFactory();
        
        }
        return instance;
    }
    
   public Vegetable getVegetable(VegetableType type, String color,double size){
       switch(type){
           case CARROT: return new Carrot(color,size);
           
           case BEET: return new Beet(color,size);
       }
      return null;

   }
    
}
